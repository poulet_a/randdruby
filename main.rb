#!/usr/bin/env ruby
#encoding: utf-8

require "rubyhelper"
require "colorize"

require_relative "dice"
require_relative "dice_roller"

if __FILE__ == $0

  case ARGV[0]
  when "carac"
    puts ARGV[2]
    Dice.puts_n_carac_series(ARGV[1].to_i, ARGV[2] == "cheat")
  when "roll"
    puts "input : '#{ARGV[1] || "1d6"}' => #{Dice.roll_dices(ARGV[1] || "1d6", ARGV[2])}"
  else
    puts "Help"
    puts "----"
    puts "roll".static(7) + " : roll dices : ".static(20) + "'roll XdX+X cheat'"
    puts "carac".static(7) + " : roll carac : ".static(20) + "'carac ntrows cheat'"
  end

end
