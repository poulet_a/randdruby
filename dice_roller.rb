#!/usr/bin/env ruby
#encoding: utf-8

require "rubyhelper"
require "colorize"

require_relative "dice"

class Dice

  def self.roll_dices(input="1d6", cheat = nil)
    return 0 if input.nil?
    raise ArgumentError, "not a valid input" unless input.is_a? String
    input.gsub(/[^\d\-\+d]/, "")
    n = input.split("d")[0].to_i
    d = input.split("d")[1].to_i
    b = input.split("+")[1].to_i
    m = input.split("-")[1].to_i
    average = d/2 * n + b - m
    ft = d/4 * n + b - m
    st = 2*d/3 * n + b - m
    r = 0
    loop do
      r = 0
      n.times{r += rand(1..d)}
      r = r + b - m
      break if cheat.nil?
      break if r >= average and cheat == "bonus"
      break if r <= average and cheat == "malus"
    end
    color = :white
    color = :red if r <= ft
    color = :green if r >= st
    return r.to_s.colorize(color)
  end

end
